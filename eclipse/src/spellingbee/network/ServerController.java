package spellingbee.network;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private SpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )*/
		
		//Returns all the 7 letters
		if(inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		
		//Returns the center letter
		if(inputLine.equals("getCenterLetter")) {
			return String.valueOf(spellingBee.getCenterLetter());
		}
		
		//Returns the current score
		if(inputLine.equals("getScore")) {
			return Integer.toString(spellingBee.getScore());	
		}
		
		//Return the bracket numbers 
		if(inputLine.equals("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			//Converting every integer in the array to a string 
			String response = "";
			for(int i : brackets) {
				response += Integer.toString(i)+";";
			}
			//Removes the last ";"
			response = response.substring(0,response.length()-1);
			return response;
		}
		
		//Return the feedback message to be displayed to the user and the number of points for a word
		String[] request = inputLine.split(";");
		if(request[0].equals("wordCheck")) {
			String points = Integer.toString(spellingBee.getPointsForWord(request[1]));
			String message = spellingBee.getMessage(request[1]);
			return message+";"+points;
		}
		
		return null;
	}
}
