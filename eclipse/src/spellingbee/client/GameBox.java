package spellingbee.client;

/**
 * Class where all the buttons, textfields are created
 * @author Liliane
 */


import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;


public class GameBox extends VBox{
	
	/**
	 * Declaring global variables so buttons/textfields can be reused or changed
	 */
	private Button[] btnArr = new Button[7];
	private TextField wordInput = new TextField("");
	private Button submit = new Button("submit");
	private Text mess = new Text("Welcome!");
	private Text points = new Text("Score : 0");
	

	/**
	 * @return the btnArr
	 */
	public Button[] getBtnArr() {
		return btnArr;
	}

	/**
	 * @return the wordInput
	 */
	public TextField getWordInput() {
		return wordInput;
	}

	/**
	 * @return the submit
	 */
	public Button getSubmit() {
		return submit;
	}

	/**
	 * @return the mess
	 */
	public Text getMess() {
		return mess;
	}

	/**
	 * @return the points
	 */
	public Text getPoints() {
		return points;
	}
	

	/**
	 * Constructor it calls the method to create the buttons and creates all the hbox and vbox
	 */
	public GameBox() {
		HBox btn = createHBox();
		HBox message = new HBox();
		
		message.getChildren().addAll(mess, points);
		message.setSpacing(20);
		wordInput.setMaxSize(200, 200);
		
		this.getChildren().addAll(btn, wordInput, submit, message);
		this.setSpacing(10);
		
	}
	
	/**
	 * Parameterized constructors where it takes as an input a HBox object
	 */
	public GameBox(HBox hb) {
		HBox message = new HBox();
		
		message.getChildren().addAll(mess, points);
		message.setSpacing(20);
		
		wordInput.setMaxSize(200, 200);
		this.getChildren().addAll(hb, wordInput, submit, message);
		this.setSpacing(10);

	}
	
	/**
	 * Method that creates the buttons and puts it in a HBox and returns the buttons created
	 * @return HBox variable
	 */
	public HBox createHBox() {
		HBox letterBtn = new HBox();
		
		for(int i = 0; i < btnArr.length; i++) {
			btnArr[i] = new Button();
			letterBtn.getChildren().addAll(btnArr[i]);
			
		}
		
		
		return letterBtn;
	}
	
	
	
}
