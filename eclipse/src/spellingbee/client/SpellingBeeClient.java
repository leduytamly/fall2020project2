package spellingbee.client;

import spellingbee.network.Client;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.TabPane;


/**
 * GUI of the SpellingBee game
 * @author Liliane
 */

public class SpellingBeeClient extends Application{
		Client client = new Client();
		/**
		 * Method where the application is formatted
		 * @param Stage object
		 * @return void 
		 */
		public void start(Stage stage) {
			/**
			 * Declaring objects needed for the GUI
			 */
			GameTab gt = new GameTab(client);
			ScoreTab st = new ScoreTab(client,gt);
			
			Group root = new Group();
			TabPane tp = new TabPane();
			
			/**
			 * Calling the methods within the GameTab class to set the letters and set a center letter
			 */
			gt.setLetters();
			gt.setCenterLetter();
			
			/**
			 * Calling the requestBrackets method inside the ScoreTab class
			 */
			st.requestBrackets();
			
			/**
			 * Inserting the tabs GameTab + ScoreTab and putting it in the group of GUI
			 */
			tp.getTabs().addAll(gt,st);
			tp.setMinWidth(650);
			tp.setMinHeight(300);
			
			root.getChildren().addAll(tp);
			
			/**
			 * Calling the methods within the GameTab class to process the letters and the submit button
			 */
			gt.processLettersToWord();	
			gt.submitWord();
			
			/**
			 * Calling the methods within the ScoreTab class to update the rank of the user
			 */
			st.updateScore();
			
			//scene is associated with container, dimensions
			Scene scene = new Scene(root, 650, 300); 
			scene.setFill(Color.WHITE);

			//associate scene to stage and show
			stage.setTitle("Spelling Bee Game"); 
			stage.setScene(scene); 
			
			stage.show(); 
			
		}
		
		/**
		 * Main method that launches the application
		 */
		public static void main(String[] args) {
	        Application.launch(args);
	    }
	}    