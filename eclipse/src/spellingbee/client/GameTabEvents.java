package spellingbee.client;

import javafx.event.*;
import javafx.scene.control.TextField;

/**
 * Game tab where the events are being handled
 * @author Liliane
 */

public class GameTabEvents implements EventHandler<ActionEvent> {
	
	public String letter;
	public TextField input;
	
	/**
	 * Constructors for the class
	 */
	public GameTabEvents() {
		letter = "";
	}
	
	/**
	 * Parameterized constructors for the class
	 * @param takes a string with the letters and the input inside the textfield
	 */
	public GameTabEvents(String letter, TextField input) {
		this.letter = letter;
		this.input = input;
	}
	
	/**
	 * Handle method that will set the TextField with what the users type
	 * @param The Event object
	 */
	public void handle(ActionEvent e) {
		input.setText(input.getText() + letter);
	}

}
