package spellingbee.client;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Tab;
import javafx.scene.paint.Color;
import spellingbee.network.Client;

/**
 * Contains all the methods for the Score tab, methods to create the layout and call the server
 * @author Le Duytam
 *
 */
public class ScoreTab extends Tab{
	private Client c;
	private ScoreBox sb = new ScoreBox();
	private GameTab gt;
	
	/**
	 * Constructor for ScoreTabe
	 * @param c Client object
	 * @param gt GameTab object
	 */
	public ScoreTab(Client c, GameTab gt) {
		super("Score");
		this.c = c;
		this.setContent(sb);
		this.gt = gt;
	}
	
	/**
	 * Request the bracket numbers from the server, and sets in the in the interface
	 */
	public void requestBrackets() {
		String response = this.c.sendAndWaitMessage("getBrackets");
		String[] responseArr = response.split(";");
		sb.setBrackets(responseArr);
		sb.setBracketInts();
	}
	
	/**
	 * Updates the score and the score bracket 
	 */
	private void refresh() {
		String response = this.c.sendAndWaitMessage("getScore");
		sb.setScore(response);
		
		int currScore = Integer.parseInt(response);
		//Checks for brackets, if score is higher than a bracket, it will set the text color to black
		for (int i = 0 ; i < sb.getBracketInts().length ; i++) {
			if (currScore >= sb.getBracketInts()[i]) {
				sb.getBracketTitles()[i].setFill(Color.BLACK);

			}
		}
	}
	
	/**
	 * Adds an event listener to the score in the GameTab to update the score in ScoreTab every time
	 * there is a change in the GameTab
	 */
	public void updateScore() {
		gt.setChangeListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String old, String newV) {
				refresh();
			}
		});
	}
}
