package spellingbee.client;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Contains all GUI elements of the ScoreTab
 * @author Le Duytam
 *
 */
public class ScoreBox extends GridPane {
	private Text[] bracketsNumbers = new Text[5];
	private int[] bracketInts = new int[5];
	private Text[] bracketTitles = new Text[6];
	private Text currScore = new Text("0");
	
	/**
	 * Constructor for ScoreBox
	 */
	public ScoreBox() {
		this.getChildren().add(createGridPane());
	}
	
	/**
	 * getter for texts for brackets
	 * @return texts array containing the tier names
	 */
	public Text[] getBracketTitles() {
		return this.bracketTitles;
	}
	
	/**
	 * getter for brackets in ints
	 * @return array of the bracket numbers in ints
	 */
	public int[] getBracketInts() {
		return this.bracketInts;
	}
	
	/**
	 * Setter for brackets numbers
	 * @param arr String array containing the tier names
	 */
	public void setBrackets(String[] arr) {
		for(int i = 0 ; i < arr.length ; i++) {
			this.bracketsNumbers[i].setText(arr[i]);
		}
	}
	
	/**
	 * Setter for brackets in ints
	 */
	public void setBracketInts() {
		this.bracketInts = convertBracketsToInt();
	}
	
	/**
	 * Setter for score
	 * @param score Score of the user
	 */
	public void setScore(String score) {
		this.currScore.setText(score);
	}
	
	/**
	 * Converts the brackets (String) to integer
	 * @return integer array of the bracket numbers
	 */
	public int[] convertBracketsToInt() {
		int[] bracketsInt = new int[this.bracketsNumbers.length];
		for(int i = 0 ; i < this.bracketsNumbers.length ; i++) {
			bracketsInt[i] = Integer.parseInt(bracketsNumbers[i].getText());
		}
		return bracketsInt;
	}
	
	/**
	 * Creates the grid pane with all the Texts and score bracket
	 * @return The gridpane with all the brackets and current score
	 */
	public GridPane createGridPane() {
		GridPane gp = new GridPane();
		String[] bracketTitles = {"Queen Bee", "Genius","Amazing","Good","Getting Started"};
		
		//Adds the title names
		for(int i = 0 ; i < bracketTitles.length ; i++) {
			this.bracketTitles[i] = new Text(bracketTitles[i]);
			this.bracketTitles[i].setFill(Color.GREY);
			gp.add(this.bracketTitles[i],0,i);
		}
		
		//Adds the current score text
		gp.add( new Text("Current Score"),0, 5);
		
		//Adds all the score bracket numbers 
		for(int i = 0 ; i < bracketTitles.length  ; i++) {
			bracketsNumbers[i] = new Text("1");
			gp.add(bracketsNumbers[i], 1, i);
		}
		
		//Adds the current score of the user
		gp.add(currScore, 1, 5);
		
		gp.setHgap(10);
		gp.setVgap(5);
		
		return gp;
	}
	

}

