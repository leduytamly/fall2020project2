package spellingbee.client;

import spellingbee.network.Client;

import javafx.scene.control.Tab;
import javafx.beans.value.ChangeListener;
import javafx.scene.paint.Color;
import javafx.event.*;

/**
 * Game tab where the client is getting called
 * @author Liliane
 */

public class GameTab extends Tab{
	
	/**
	 * Declaring the client and objects
	 */
	private Client c;
	
	GameBox gb = new GameBox();
	GameTabEvents gte = new GameTabEvents();
	
	/**Parameterized constructor with the client 
	 * @param Client c from the Client class
	 */
	public GameTab(Client c) {
		super("Game");
		this.c = c;
		this.setContent(gb);
		gb.getSubmit().setDefaultButton(true);
	}
	
	/** Method that sends an input line to the client to get all the letters needed for the game
	 * The response will then be set in the buttons
	 */
	public void setLetters() {
		String response = c.sendAndWaitMessage("getAllLetters"); 
		//System.out.println(response);
		
		for(int i = 0 ; i < response.length(); i++) {
			gb.getBtnArr()[i].setText(String.valueOf(response.charAt(i))); 
			
		}

	}
	
	/** Method that sends an input line to the client to get the center letter among the letters chosen
	 * The response will then show which letter button is chosen
	 */
	public void setCenterLetter() {
		String response = c.sendAndWaitMessage("getCenterLetter");
		
		for(int i = 0 ; i < gb.getBtnArr().length; i++) {
			
			//System.out.println(gb.btnArr[i].getText().equals(response));
			
			if (gb.getBtnArr()[i].getText().equals(response)) {
				gb.getBtnArr()[i].setTextFill(Color.RED);
			}	
		}
	}
	
	/** Method that sets the events on each button
	 */
	public void processLettersToWord() {
		
		for(int i = 0; i < gb.getBtnArr().length; i++) {
				gte = new GameTabEvents(gb.getBtnArr()[i].getText(), gb.getWordInput());
				gb.getBtnArr()[i].setOnAction(gte);
			
		}

	}
	
	/**Method that sends an input line to the client to check if the word makes sense
	 * It will also send an input line to get the score
	 * It is done in an anonymous function to 
	 * The response will then put the result in the TextFields
	 */
	public void submitWord() {
		gb.getSubmit().setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!(gb.getWordInput().getText().equals(""))) {
					String request = "wordCheck;" + gb.getWordInput().getText().toLowerCase();
					String response = c.sendAndWaitMessage(request);
					
					String response2 = c.sendAndWaitMessage("getScore");

					String[] splitArr = response.split(";");

					gb.getMess().setText(splitArr[0]);
					gb.getPoints().setText("Score : " + response2);
				} else {
					gb.getMess().setText("Please input a word!");
				}

				
				gb.getWordInput().setText("");
			}
			
		});
	}
	
	/**
	 * Sets a change listener to the points field
	 * @param e ChangeListener to attach to the points field
	 * @author Le
	 */
	public void setChangeListener(ChangeListener<String> e) {
		gb.getPoints().textProperty().addListener(e);
	}
	
}
