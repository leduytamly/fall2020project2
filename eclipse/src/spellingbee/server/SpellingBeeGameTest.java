package spellingbee.server;

import spellingbee.network.*;

import java.io.IOException;
import java.util.HashSet;

public class SpellingBeeGameTest {
	public static void main(String[] args) throws IOException {
		/*SpellingBeeGame sbg = new SpellingBeeGame("uablgyr");
		
		//Checking if the method read the english.txt file
		/*for (String s : SpellingBeeGame.possibleWords) {
			System.out.println(s);
		}
		
		
		int[] brackets = sbg.getBrackets();
		
		System.out.println();
		
		//Testing getAllLetters
		System.out.println(sbg.getAllLetters());
		//Testing getCenterLetter
		System.out.println(sbg.getCenterLetter());
		//Testing getScore
		System.out.println(sbg.getScore());
		
		//Printing the bracket
		System.out.println();
		for(int i : brackets) {
			System.out.println(i);
		}
		*/
		
		ServerController sc = new ServerController();
		System.out.println(sc.action("getAllLetters"));
		System.out.println(sc.action("getCenterLetter"));
		System.out.println(sc.action("getScore"));
		System.out.println(sc.action("getBrackets"));
		System.out.println(sc.action("wordCheck;ging"));
	}
}
