package spellingbee.server;

/**
 * Simplified Spelling Bee game for the GUI testing purposes
 * @author Liliane
 */

public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	
	/**
	 * Declaring variables
	 */
	private char[] letters = new char[7];
	private int score;
	private String[] words = new String[5];

	/**
	 * Constructor with hardcoded values
	 */
	public SimpleSpellingBeeGame() {
		letters[0] = 'i';
		letters[1] = 'e';
		letters[2] = 'g';
		letters[3] = 'p';
		letters[4] = 'l';
		letters[5] = 's';
		letters[6] = 'n';
		
		this.score = 0;
		words = setWords();
	}
	
	/**
	 * Method that calculates the points
	 * @param takes in the String attempt
	 */
	public int getPointsForWord(String attempt) {
		if(getMessage(attempt).equals("good")) {
			this.score += 10;
		}
		else {
			this.score += 0;
		}
		
		return this.score;
	}
	
	/**
	 * Getter which loops through the word array of correct words and returns if it is valid or not
	 * @param String of the word that the user input
	 */
	public String getMessage(String attempt) {
		
		for(int i = 0 ; i < words.length; i++) {
			
			if(attempt.equals(words[i])) {
				return ("good");
			}
		}
		return ("bad");
	}
	
	/**
	 * Setter to put the correct words in the array
	 */
	public String[] setWords() {
		words[0]= "single";
		words[1]= "spline";
		words[2]= "slip";
		words[3]= "spine";
		words[4]= "snip";
		
		return words;
	}
	
	/**
	 * Getter that gets all the letters for the buttons
	 */
	public String getAllLetters() {
		return new String(this.letters);
		
	}
	
	/**
	 * Getter that gets the center letter
	 */
	public char getCenterLetter() {
		return 'i';
		
	}
	
	/**
	 * Getter that gets the score
	 */
	public int getScore() {
		return this.score;
	}
	
	/**
	 * Getter that gets brackets score
	 */
	public int[] getBrackets() {
		int[] brackets = new int[5];
		brackets[0] = 40;
		brackets[1] = 30;
		brackets[2] = 20;
		brackets[3] = 10;
		brackets[4] = 5;
		
		return brackets;
	}
}
