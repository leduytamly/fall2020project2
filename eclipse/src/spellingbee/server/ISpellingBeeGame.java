package spellingbee.server;

/**
 * Stores all required methods for a Spelling Bee game 
 * @author Le Duytam, Liliane
 *
 */
public interface ISpellingBeeGame {
	/**
	 * Returns the number of points that a given word is worth according to the Spelling Bee rules
	 * @param attempt Given word to be checked
	 * @return Number of points the word is worth
	 */
	int getPointsForWord(String attempt);
	
	/**
	 * Checks if the word attempt is valid or not and returns a message saying saying whether the word is good or bad
	 * @param attempt Given word to be checked
	 * @return Message saying if the word is good or bad 
	 */
	String getMessage(String attempt);
	
	/**
	 * Returns a set of 7 letters for the user to use
	 * @return String containing 7 characters 
	 */
	String getAllLetters();
	
	/**
	 * Returns a character to be the center letter (required letter in a word)
	 * @return A character
	 */
	char getCenterLetter();
	
	/**
	 * Returns the total score of the user
	 * @return The total score of the user 
	 */
	int getScore();
	
	/**
	 * Gets the number of points for each brackets
	 * @return Array of all the bracket numbers
	 */
	int[] getBrackets();
}
