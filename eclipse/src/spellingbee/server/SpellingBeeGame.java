package spellingbee.server;

import java.util.HashSet;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;

/**
 * Contains all the information and methods necessary to play a Spelling Bee game
 * @author Le Duytam
 *
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	private char[] letters;
	private char centerLetter;
	private int score;
	private HashSet<String> wordsFound;
	public static HashSet<String> possibleWords = createWordsFromFile("src/datafiles/english.txt");
	private int[] brackets;
	
	/**
	 * Static Method which reads all the lines from a source and returns a HashSet with all the lines
	 * @return HashSet which contains all lines of a file
	 */
	private static HashSet<String> createWordsFromFile(String path){
		try {
			Path p = Paths.get(path);
			List<String> lines = Files.readAllLines(p);
			HashSet<String> set = new HashSet<String>();
			set.addAll(lines);
			return set;
		} catch (IOException e) {
			System.out.println("There was an IOException");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Constructor with no parameters
	 */
	public SpellingBeeGame(){
		try {
			this.letters = setAllLetters();
			this.centerLetter = setRandomCenterLetter();
			this.score = 0;
			this.wordsFound = new HashSet<String>();
			this.brackets = calculateBracket();
		} catch (IOException e) {
			System.out.println("There was an IOException");
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor with parameters
	 * @param s String to be put into letters
	 */
	public SpellingBeeGame(String s) {
		this.letters = convertStringToCharArray(s);
		this.centerLetter = setRandomCenterLetter();
		this.score = 0;
		this.wordsFound = new HashSet<String>();
		this.brackets = calculateBracket();
	}
	
	/**
	 * Returns the total points for a given words
	 * Uses the tabulatePoints method 
	 * @return Number of points given for a word
	 */
	@Override
	public int getPointsForWord(String attempt) {
		if (!isValidWord(attempt) || !(isWordInDictionary(attempt)) || isWordInWordsFound(attempt)) {
			return 0;
		}
		this.score += tabulatePoints(attempt);
		return tabulatePoints(attempt);
	}

	/**
	 * Returns "good" if the word given is valid and "bad" if the word given is bad
	 * if the word is valid, adds to the wordsFound HashSet
	 * @return String indicating if the word was good or bad
	 */
	@Override
	public String getMessage(String attempt) {
		if(isPanagram(attempt)) {
			return "Amazing that was a Panagram!";
		}
		if(isWordInWordsFound(attempt)) {
			return "Word already guessed!";
		}
		if (isValidWord(attempt) && isWordInDictionary(attempt)) {
			wordsFound.add(attempt);
			return "Good!";
		}
		if(attempt.length() < 4) {
			return "Word is too short!";
		}
		return "Not a word!";
	}

	/**
	 * Getter for letters
	 */
	@Override
	public String getAllLetters() {
		return new String(this.letters);
	}

	/**
	 * Getter for centerLetter
	 */
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}

	/**
	 * Getter for score
	 */
	@Override
	public int getScore() {
		return this.score;
	}

	/**
	 * Getter for brackets
	 */
	@Override
	public int[] getBrackets() {
		return this.brackets;
	}
	
	/**
	 * Returns a char array of random letters from letterCombinations.txt file 
	 * @return
	 * @throws IOException
	 */
	private char[] setAllLetters() throws IOException {
		Path p = Paths.get("src/datafiles/letterCombinations.txt");
		List<String> lines = Files.readAllLines(p);
		
		//Gets a random line from the file
		Random randgen = new Random();
		String lettersString = lines.get(randgen.nextInt(lines.size()));
		
		//Converting the String into a char[]
		char[] letterChars = convertStringToCharArray(lettersString);
		
		return letterChars;
	}
	
	/**
	 * Methods that takes a String and returns a char array of that String
	 * @param s String to be converted to char array
	 * @return char array of the string
	 */
	private char[] convertStringToCharArray(String s) {
		char[] letterChars = new char[s.length()];
		for(int i = 0 ; i < s.length() ; i++) {
			letterChars[i] = s.charAt(i);
		}
		return letterChars;
	}
	
	/**
	 * Return a random character from the letters array
	 * @return return a random letter in the letters array
	 */
	private char setRandomCenterLetter() {
		Random randgen = new Random();
		return this.letters[randgen.nextInt(this.letters.length)];
	}
	
	/**
	 * Calculates the total possible points with the letters and determines the point brackets
	 * @return integer array which contains the brackets to be used in front end
	 */
	private int[] calculateBracket() {
		final int MIN_LENGTH_FOR_WORD = 4;
		int totalPoints = 0;
		//Loops through all the words in possibleWords
		for (String s : possibleWords) {
			//Skips all words with length greater than 7 or length equal to 1
			if(s.length() < MIN_LENGTH_FOR_WORD) {
				continue;
			}
			//if all characters word is in the character array and the center letter is present, it will calculate the points 
			if(isValidWord(s)) {
				totalPoints += tabulatePoints(s);
			}
		}
		
		//Creates the array for the bracket (100%,90%,75%,50%,25%) 
		int[] bracket = {totalPoints,90*totalPoints/100,3*totalPoints/4,totalPoints/2,(totalPoints/4)}; 
		return bracket;
	}
	
	/**
	 * Calculates the point given for a word
	 * @param wordLength the length of the word 
	 * @return the total number of points for the word
	 */
	private int tabulatePoints(String word) {
		final int BONUS = 7;
		int points = 0;
		//If the length less than 4, 
		if(word.length() < 4) {
			points=0;
		//If the length is 4, 4 points given
		}else if(word.length() == 4) {
			points = 1;
		//If it is a panagram, there is 7 points bonus
		} else if (isPanagram(word)){
			points = word.length() + BONUS;
		//If it is greater than 5 and is not a panagram, there is no bonus
		} else if(word.length() > 4) {
			points = word.length();
		}
		return points;
	}
	
	/**
	 * Checks if the word is valid, center letter is in the word and if every letter of the word is in the letters array
	 * @param word Word given
	 * @return boolean value representing if the word is valid or not
	 */
	private boolean isValidWord(String word) {
		//If the center letter is not found, returns false 
		if(word.indexOf(centerLetter)==-1) {
			return false;
		}
		//if the length is less than 4, the word is not valid
		if(word.length() < 4) {
			return false;
		}
		//Counts if every letter in the word appears in the letters array
		int count = 0;
		for(int i = 0 ; i < word.length() ; i++) {
			for(int j = 0 ; j < 7 ; j++) {
				if(word.charAt(i) == letters[j]) {
					count++;
					break;
				}
			}
		}
		//If the count is the same the word length, it means that you can make the word with the given letters
		return word.length() == count;
	}
	
	/**
	 * Checks if a word is a panagram
	 * @param word Word to check
	 * @return boolean value representing if the word is a a panagram or not
	 */
	private boolean isPanagram(String word) {
		int count = 0;
		//Counts if every letter in the char array is in the word
		for(int i = 0 ; i < letters.length; i++) {
			for(int j = 0 ; j <   word.length(); j++) {
				if(word.charAt(j) == letters[i]) {
					count++;
					break;
				}
			}
		}
		//If count is the same as letters.length, it means that all letters are used (panagram)
		return count == letters.length;
	}
	
	/**
	 * Checks if the word is in the dictionary
	 * @param word Word to check
	 * @return boolean value representing if the word is in the dictionary or not
	 */
	private boolean isWordInDictionary(String word) {
		return possibleWords.contains(word);
	}
	
	/**
	 * Checks if the word is in the words found hashset
	 * @param word
	 * @return boolean value representing if the word is already guessed
	 */
	private boolean isWordInWordsFound(String word) {
		return wordsFound.contains(word);
	}
}
